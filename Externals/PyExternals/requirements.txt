# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# List of python modules to install as part of the statistics release.
#

pyhf==0.7.4
ipython==8.2.0
ipykernel==6.15.0
gnureadline==8.1.2
metakernel==0.29.0
iminuit==2.17.0
pytest==7.1.3
pytest-order
