import ROOT; ROOT.gROOT.SetBatch(False);
from ROOT import gROOT,gDirectory
import argparse


from sys import argv
if len(argv)>1:
    print(argv)
    from IPython import get_ipython
    ipython = get_ipython()
    for a in argv[1:]:
        if a.endswith(".root"):
            print("Loading",a)
            ROOT.gROOT.GetListOfFiles().Add(ROOT.TFile.Open(a))
        else:
            print("Running ",a)
            ipython.run_line_magic("run",a)
            